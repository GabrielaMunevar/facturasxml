from msilib.schema import Class
import os
import json
import xmltodict
import pandas as pd
import sqlalchemy as db
from base import Base
import openpyxl

book = openpyxl.load_workbook('excel.xlsx', data_only=True)
hoja = book.active 

celdas = hoja['H2': 'H110301']

lista_xml=[]
for fila in celdas:
  for celda in fila:
    xml = celda.value
    #xml= [celda.value for celda in fila]
    lista_xml.append(xml)

class ExcelProcess(Base):
    def __init__(self,process_path,finished_path,error_path):
        super().__init__()
        #Checks if process_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(process_path)
        #Checks if finished_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(finished_path)
        #Checks if error_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(error_path)

   # def downloadXML(self,) 

class XMLProcess(Base):
    def __init__(self,process_path,finished_path,error_path):
        super().__init__()
        #Checks if process_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(process_path)
        #Checks if finished_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(finished_path)
        #Checks if error_path folder exists and if it doesn't exist then creates the folder
        self.createFolder(error_path)
        
    def extractDataFromXMLFile(self,xml_filename_path):          
        #Open XML File
        with open((xml_filename_path), "rb") as f:
            xml_txt = f.read().decode('utf8')
        xml_json = json.loads(json.dumps(xmltodict.parse(xml_txt)))
        nombre_factura=os.path.basename(xml_filename_path).split('.')[0]
        #Genenal Invoice Information
        #Información Factura

        json_comprobante        =   xml_json['nfeProc']   #        
        version_xml           =   json_comprobante['@versao']  
        XML_ns                  =   json_comprobante['@xmlns']
        nota_fiscal             =   json_comprobante['NFe'] #
        info_nota_fiscal        =   nota_fiscal['infNFe']  #
        id_xml                  =   info_nota_fiscal['@Id']
        #Info nota fiscal
        info_identificacion_nf  =   info_nota_fiscal['ide'] #
        codigo_uf_emisor        =   info_identificacion_nf['cUF']
        naturaleza_operacion    =   info_identificacion_nf['natOp']
        modelo_codigo_df        =   info_identificacion_nf['mod']
        serie_df                =   info_identificacion_nf['serie']
        numero_df               =   info_identificacion_nf['nNF']
        fecha_factura           =   info_identificacion_nf['dhEmi']
        tipo_operacion          =   info_identificacion_nf['tpNF']
        identificador_destino   =   info_identificacion_nf['idDest']
        codigo_municipio_fg     =   info_identificacion_nf['cMunFG']
        formato_impresion_DANFE =   info_identificacion_nf['tpImp']
        tipo_emision            =   info_identificacion_nf['tpEmis']
        digito_verificador_nf   =   info_identificacion_nf['cDV']
        identificacion_ambiente =   info_identificacion_nf['tpAmb']
        finalidad_emision       =   info_identificacion_nf['finNFe']
        oper_consumidor_final   =   info_identificacion_nf['indFinal']
        presencia_comprador     =   info_identificacion_nf['indPres']
        proceso_emision         =   info_identificacion_nf['procEmi']
        version_proceso_emision =   info_identificacion_nf['verProc']

        #información emisor
        emisor                  =   info_nota_fiscal['emit'] #
        cnpj_emisor             =   emisor['CNPJ']
        razon_social_emisor     =   emisor['xNome']
        nombre_fantasia         =   emisor['xFant'] ##viene con '******'

        #Direccion emisor
        direccion               =   emisor['enderEmit']#
        direccion_emisor        =   direccion['xLgr']
        numero_dir              =   direccion['nro']
        distrito                =   direccion['xBairro']
        codigo_municipio        =   direccion['cMun']
        nombre_municipio        =   direccion['xMun']
        sigla_uf                =   direccion['UF']
        codigo_cep              =   direccion['CEP']
        codigo_pais             =   direccion['cPais']
        nombre_pais             =   direccion['xPais']
        telefono_emisor         =   direccion['fone']

        # ITEMS de la factura

        detalle_factura         =   info_nota_fiscal['det'] #


        numero_items           =   len(detalle_factura)
        productos = detalle_factura
        df_productos=[]
        if not isinstance(productos, list):
            productos = [productos]
        for producto in productos:
            df_productos.append(pd.json_normalize(producto, max_level=2))
        productos_factura = pd.concat(df_productos, ignore_index=True)


        #Total
        total_factura              =    info_nota_fiscal['total']
        icms_total             =    total_factura['ICMSTot']

        #icms_total = pd.json_normalize(icms_total,max_level=2)
        vbc                 = icms_total['vBC']
        vicms               = icms_total['vICMS']
        vicmsdeson          = icms_total['vICMSDeson']
        vfcpufdest          = icms_total['vFCPUFDest']
        vicmsufdest         = icms_total['vICMSUFDest']
        vicmsufremet        = icms_total['vICMSUFRemet']
        vfcp                = icms_total['vFCP']
        vbcst               = icms_total['vBCST']
        vst                 = icms_total['vST']
        vfcpst              = icms_total['vFCPST']
        vfcpstret           = icms_total['vFCPSTRet']
        vprod               = icms_total['vProd']
        vfrete              = icms_total['vFrete']
        vseg                = icms_total['vSeg']
        vdesc               = icms_total['vDesc']
        vii                 = icms_total['vII']
        vipi                = icms_total['vIPI']
        vipidevol           = icms_total['vIPIDevol']
        vpis                = icms_total['vPIS']
        vcofins             = icms_total['vCOFINS']
        voutro              = icms_total['vOutro']
        vnf                 = icms_total['vNF']
        vtottrib            = icms_total['vTotTrib']


        impuesto_icms00         = None if  'imposto.ICMS.ICMS00' not in productos_factura else productos_factura['imposto.ICMS.ICMS00']
        impuesto_icms60         = None if  'imposto.ICMS.ICMS60' not in productos_factura else productos_factura['imposto.ICMS.ICMS60']
        impuesto_pisaliq       = None if  'imposto.PIS.PISAliq' not in productos_factura else productos_factura['imposto.PIS.PISAliq']
        impuesto_cofinsaliq    = None if  'imposto.COFINS.COFINSAliq' not in productos_factura else productos_factura['imposto.COFINS.COFINSAliq']
        impuesto_pisnt         = None if  'imposto.PIS.PISNT' not in productos_factura else productos_factura['imposto.PIS.PISNT']
        impuesto_cofinsnt      = None if  'imposto.COFINS.COFINSNT' not in productos_factura else productos_factura['imposto.COFINS.COFINSNT']


        productos_factura['fecha_factura'] = fecha_factura
        productos_factura['nombre_factura'] = numero_df
        productos_factura['version_xml'] = version_xml
        productos_factura['id_xml'] = id_xml
        productos_factura['codigo_uf_emisor '] = codigo_uf_emisor 
        productos_factura['naturaleza_operacion'] = naturaleza_operacion
        productos_factura['modelo_codigo_df'] = modelo_codigo_df
        productos_factura['serie_df'] = serie_df
        productos_factura['numero_df'] = numero_df
        productos_factura['tipo_operacion'] = tipo_operacion
        productos_factura['identificador_destino'] = identificador_destino
        productos_factura['codigo_municipio_fg'] = codigo_municipio_fg
        productos_factura['formato_impresion_DANFE'] = formato_impresion_DANFE
        productos_factura['tipo_emision'] = tipo_emision
        productos_factura['digito_verificador_nf'] = digito_verificador_nf
        productos_factura['identificacion_ambiente'] = identificacion_ambiente
        productos_factura['finalidad_emision'] = finalidad_emision
        productos_factura['oper_consumidor_final'] = oper_consumidor_final
        productos_factura['presencia_comprador'] = presencia_comprador
        productos_factura['proceso_emision'] = proceso_emision
        productos_factura['version_proceso_emision'] = version_proceso_emision
        productos_factura['cnpj_emisor'] = cnpj_emisor
        productos_factura['razon_social_emisor'] = razon_social_emisor
        productos_factura['nombre_fantasia'] = nombre_fantasia
        productos_factura['direccion_emisor'] = direccion_emisor
        productos_factura['numero_dir'] = numero_dir
        productos_factura['distrito'] = distrito
        productos_factura['codigo_municipio'] = codigo_municipio
        productos_factura['nombre_municipio'] = nombre_municipio
        productos_factura['sigla_uf'] = sigla_uf
        productos_factura['codigo_cep'] = codigo_cep
        productos_factura['codigo_pais'] = codigo_pais
        productos_factura['telefono_emisor'] = telefono_emisor

        productos_factura['vbc'] = vbc
        productos_factura['vicms'] = vicms
        productos_factura['vicmsdeson'] = vicmsdeson
        productos_factura['vfcpufdest']= vfcpufdest
        productos_factura['vicmsufdest'] =vicmsufdest
        productos_factura['vicmsufremet']= vicmsufremet
        productos_factura['vfcp']= vfcp
        productos_factura['vbcst']= vbcst
        productos_factura['vst'] =vst
        productos_factura['vfcpst'] =vfcpst
        productos_factura['vfcpstret']= vfcpstret
        productos_factura['vprod'] =vprod
        productos_factura['vfrete']= vfrete
        productos_factura['vseg']= vseg
        productos_factura['vdesc']= vdesc
        productos_factura['vii']= vii
        productos_factura['vipi']= vipi
        productos_factura['vipidevol']=vipidevol
        productos_factura['vpis']=vpis
        productos_factura['vcofins']=vcofins
        productos_factura['voutro']=voutro
        productos_factura['vnf']=vnf
        productos_factura['vtottrib']=vtottrib


        #productos_factura1= productos_factura.merge(icms_total, left_on='telefono_emisor', right_on='vBC')
        #productos_factura = pd.merge([productos_factura, icms_total], how="right_on=fecha_factura")



        if impuesto_icms60 is None:
            productos_factura['imposto.ICMS.ICMS60'] = impuesto_icms60

        if impuesto_icms00 is None:
            productos_factura['imposto.ICMS.ICMS00'] = impuesto_icms00

        if impuesto_cofinsaliq is None:
            productos_factura['imposto.COFINS.COFINSAliq'] = impuesto_cofinsaliq

        if impuesto_pisaliq is None:
            productos_factura['imposto.PIS.PISAliq'] = impuesto_pisaliq

        if impuesto_cofinsnt is None:
            productos_factura['imposto.COFINS.COFINSNT'] = impuesto_cofinsnt

        if impuesto_pisnt is None:
            productos_factura['imposto.PIS.PISNT'] = impuesto_pisnt

        #Renombrar columnas
        columns_names = { '@nItem':'numero_item',
                        'prod.cProd':'codigo_producto',
                        'prod.cEAN':'gtin_codigo_ean',
                        'prod.xProd':'descripcion_producto',
                        'prod.NCM':'codigo_ncm', 
                        'prod.CFOP':'codigo_fiscal_operaciones',
                        'prod.uCom':'unidad_comercial',
                        'prod.qCom': 'cantidad_comercial',
                        'prod.vUnCom':'valor_unitario',
                        'prod.vProd':'valor_total_producto',
                        'prod.cEANTrib':'gtin_imponible',
                        'prod.uTrib':'unidad_imponible',
                        'prod.qTrib':'cantidad_imponible',
                        'prod.vUnTrib':'unidad_valor_impuesto',
                        'prod.indTot':'valor_incluido_nfe',
                        'imposto.vTotTrib':'valor_total_impuesto',
                        'imposto.ICMS.ICMS60':'impuesto_icms60',
                        'imposto.ICMS.ICMS00':'impuesto_icms00',
                        'imposto.PIS.PISAliq':'impuesto_pisaliq',
                        'imposto.COFINS.COFINSAliq':'impuesto_cofinsaliq',
                        'imposto.PIS.PISNT':'impuesto_pisnt',
                        'imposto.COFINS.COFINSNT':'impuesto_cofinsnt'
        }
        productos_factura.rename(columns = columns_names, inplace = True)



        columns_selected=[
                        'fecha_factura',
                        'nombre_factura',
                        'version_xml',
                        'id_xml',
                        'naturaleza_operacion',
                        'modelo_codigo_df',
                        'serie_df',
                        'numero_df',
                        'tipo_operacion',
                        'digito_verificador_nf',
                        'identificacion_ambiente',
                        'finalidad_emision',
                        'oper_consumidor_final',
                        'presencia_comprador',
                        'proceso_emision',
                        'version_proceso_emision',
                        'cnpj_emisor',
                        'razon_social_emisor',
                        'nombre_fantasia',
                        'direccion_emisor',
                        'numero_dir',
                        'distrito',
                        'codigo_municipio',
                        'nombre_municipio',
                        'sigla_uf',
                        'codigo_cep',
                        'codigo_pais',
                        'telefono_emisor',
                        'numero_item',
                        'codigo_producto',
                        'gtin_codigo_ean',
                        'descripcion_producto',
                        'codigo_ncm',
                        'codigo_fiscal_operaciones',
                        'unidad_comercial',
                        'cantidad_comercial',
                        'valor_unitario',
                        'valor_total_producto',
                        'gtin_imponible',
                        'unidad_imponible',
                        'cantidad_imponible',
                        'unidad_valor_impuesto',
                        'valor_incluido_nfe',
                        'valor_total_impuesto',
                        'impuesto_icms60',
                        'impuesto_icms00',
                        'impuesto_pisaliq',
                        'impuesto_cofinsaliq',
                        'impuesto_pisnt',
                        'impuesto_cofinsnt',
                        'vbc',
                        'vicms',
                        'vicmsdeson',
                        'vfcpufdest',
                        'vicmsufdest',
                        'vicmsufremet', 
                        'vfcp',
                        'vbcst',
                        'vst',
                        'vfcpst',
                        'vfcpstret',
                        'vprod',
                        'vfrete',
                        'vseg',
                        'vdesc',
                        'vii',
                        'vipi',
                        'vipidevol',
                        'vpis',
                        'vcofins',
                        'voutro',
                        'vnf',
                        'vtottrib'
                        ]

        productos_factura = productos_factura[columns_selected]
        return productos_factura

    def uploadDataToDatabe(self,productos_factura):
        #Upload Dataframe into DataBase
        productos_factura.to_sql('sales_invoices_brasil', con = self.engine, if_exists = 'append', dtype={"impuesto_icms60" : db.types.JSON,"impuesto_icms00" : db.types.JSON, "impuesto_pisaliq" : db.types.JSON, "impuesto_cofinsaliq" : db.types.JSON, "impuesto_pisnt" : db.types.JSON, "impuesto_cofinsnt" : db.types.JSON }, index= False)
            
       





