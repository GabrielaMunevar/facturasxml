
from setup_logger import logger
from config import Config
from xmlprocess import XMLProcess
import os
import shutil


def loadConfigurations():
    try:
        print("Cargando configuraciones")
        logger.info('loadConfigurations: Loading settings')
        #Load settings from args file
        config = Config()      
        return config.process()
    except:
        print("Error cargando configuraciones")
        logger.error('loadConfigurations: Error loading settings')

if __name__ == "__main__":
    #Init log file
    logger.info('Main: Starting process')
    #Load Configurations from settings file
    settings = loadConfigurations()
    #Init folders needed to run the process
    xmlprocess = XMLProcess(settings["process_path"],settings["finished_path"],settings["error_path"])
    #Iter over files in source_path
    for xml_file in os.listdir(settings["source_path"]):
        #Print Xml file name
        print(xml_file)
        #Build Xml file path
        xml_file_path       = os.path.join(settings["source_path"], xml_file)
        #Build Xml file path where the file will be placed once it has been processed
        xml_finisted_path   = os.path.join(settings["finished_path"], xml_file) 
        #Build Xml file path where the file will be placed in case of error
        xml_error_path      = os.path.join(settings["error_path"], xml_file)
        try:
            #Extract information from XML File into a DataFrame
            df_factura=xmlprocess.extractDataFromXMLFile(xml_file_path)
            #Print Extracted Data
            print(df_factura)
            #Upload information from Dataframe to DataBase
            xmlprocess.uploadDataToDatabe(df_factura)
            #Move xml file from source_path to finished_path
            shutil.move(xml_file_path, xml_finisted_path)
            #Move xml file from source_path to finished_path
            logger.info('Main: Processing file '+ xml_file)
        except:
            logger.error('Main: Error processing file '+ xml_file)
            shutil.move(xml_file_path, xml_error_path)
            




