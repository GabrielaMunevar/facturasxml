import logging
import os
from datetime import datetime

#Checks if log folder exists 
Exists = os.path.exists('logs')
#If log folder doesn't exist it creates the folder
if not Exists:
    os.makedirs('logs')

#Get today's date in order to name the log file
today=datetime.now().strftime('%Y_%m_%d')

#Set loggeer configuration
logging.basicConfig(filename=f'logs/rappi-brasil-{today}.log', 
    level=logging.INFO, 
    format='%(asctime)s | %(name)s | %(levelname)s | %(message)s')
logger = logging.getLogger('rappi brasil')