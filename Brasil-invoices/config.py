from setup_logger import logger
import sys
import json


class Config():
    def __init__(self):
        super().__init__()

    def process(self):
        if len(sys.argv) != 2:
            print(f'Usage: python {sys.argv[0]} <config-file>')
            sys.exit(1)

        configFilename = sys.argv[1]
        config = {} 
        try:      
            print("configFile",configFilename)
            with open(configFilename) as configFile:
                config = json.load(configFile)
                logger.info('Config: Settings loaded successfully')   
                return config            
        except Exception as e:
            logger.error('Config: Error loading settings :'+e)
            raise(e)




