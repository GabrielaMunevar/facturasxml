import os
import json
import pymysql
import shutil
import requests
import logging
import time
from setup_logger import logger
import pyautogui
import pandas as pd
import sqlalchemy as db
import psycopg2
from datetime import datetime


class Base:
    def __init__(self):
        # Conexión a Base de Datos
        self.strConnection = 'postgresql+psycopg2://postgres:Admin.Prevalentware@prevalent-db.c3rkad1ay1ao.us-east-1.rds.amazonaws.com:5432/rappi'
        self.engine = db.create_engine(self.strConnection)
        connection = self.engine.connect()
    
    def query(self,query):
        return pd.read_sql_query(query,self.connection)

    def mutate(self,query):
        self.connection.execute(query)

    def createFolder(self,folder):
        #Checks if folder exists
        Exists = os.path.exists(folder)
        #If folder doesn't exist it creates the folder
        if not Exists:
            os.makedirs(folder) 
    
    def screenshot(self,filename,folder):
        #Take current year, month and day 
        year  = datetime.now().strftime('%Y')
        month = datetime.now().strftime('%m')
        day   = datetime.now().strftime('%d')
        #Define screenshot name using current date information
        screenshot_name= filename+'-'+datetime.now().strftime('%Y_%m_%d_%H%M')+'.png'
        #Define screenshot folder path and file path
        screenshot_folder_path = os.path.join(folder, year,month,day)
        screenshot_file_path = os.path.join(screenshot_folder_path,screenshot_name)
        #Checks if folder exists, if folder doesn't exist it creates the folder
        self.createFolder(screenshot_folder_path)
        #Save screenshot in folder 
        screenshot = pyautogui.screenshot()
        screenshot.save(screenshot_file_path)      
                        
    def executeScriptsFromFile(self,filepath):
        #Open and read the file as a single buffer
        fd = open(filepath, 'r')
        sqlFile = fd.read()
        fd.close()
        #Split SQL file by SQL Commands using ';' separator
        sqlCommands = sqlFile.split(';')
        # Execute every command from the input file
        for command in sqlCommands:
            # This will skip and report errors
            try:
                self.connection.execute(command)
            except:
                print("Error ejecutando comando")

    def cleanDownloadFolder(self,input_folder):
        for downloaded_report in os.listdir(input_folder): 
            input_filename_path = os.path.join(input_folder, downloaded_report)
            os.remove(input_filename_path)
            logger.info(f'cleanDownloadFolder: {input_filename_path} deleted')

    def waitDownload(self,input_folder,file_number=None):
        seconds = 0
        dl_wait = True
        while dl_wait and seconds < 20:
            time.sleep(1)
            dl_wait = False 
            reports = os.listdir(input_folder)
            if file_number and len(reports) != file_number:
                dl_wait = True 
            # Iterate over reports in folder     
            for downloaded_report in reports:         
                # ".crdownload" if a suffix appended to the name of the file by Google Chrome if it's not completely downloaded
                if downloaded_report.endswith('.crdownload'): 
                    dl_wait = True
            seconds += 1
            print('waiting for report',downloaded_report)
            print('file number',file_number)
            print('len(reports)',reports)
        return seconds
     
    def moveReports(self,category, input_folder, output_folder):
        #Build the folder name depending on category (Power or Energy)
        folder_output_type=output_folder+"\\"+category
        #Checks if folder exists and if it doesn't exist then creates the folder
        self.createFolder(folder_output_type) 
        seconds = self.waitDownload(input_folder,1)
        if  seconds > 20:
            logger.info(f'moveReports: timeout downloading report for {category}')       
            raise()
        else :
            for downloaded_report in os.listdir(input_folder): 
                input_filename_path = os.path.join(input_folder, downloaded_report)
                output_filename_path = os.path.join(folder_output_type, downloaded_report)      
                if ('eToday' in input_filename_path) and (category == 'Energy'):
                    #Move file from download folder into category folder 
                    shutil.move(input_filename_path, output_filename_path)
                    logger.info(f'moveReports: {input_filename_path} moved to Energy Folder')
                elif ('activePower' in input_filename_path) and (category == 'Power'):
                    #Move file from download folder into category folder 
                    shutil.move(input_filename_path, output_filename_path)
                    logger.info(f'moveReports: {input_filename_path} moved to Power Folder')
                else:
                    logger.info(f'moveReports: {input_filename_path} removed')
                    os.remove(input_filename_path)
                    raise()
                                 
                    
    def getSharePointToken(self):
        url = "https://accounts.accesscontrol.windows.net/"+self.sharepoint_tenant_id+"/tokens/OAuth/2"
        payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"grant_type\"\r\n\r\nclient_credentials\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_id\t\"\r\n\r\n"+self.sharepoint_client_id+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_secret\"\r\n\r\n"+self.sharepoint_client_secret+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"resource\t\"\r\n\r\n00000003-0000-0ff1-ce00-000000000000/"+self.sharepoint_tenant_name+".sharepoint.com@"+self.sharepoint_tenant_id+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"      
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'accept': "application/x-www-form-urlencoded",
            'cache-control': "no-cache",
            }
        try:
            response = requests.post(url, data=payload, headers=headers)
            token_response = json.loads(response.content)
            self.sharepoint_token=token_response['access_token']
        except:
            raise Exception('Error requesting sharepoint auth token')

    def uploadFile(self,site,folderSP,file):
        #Carga de documentos procesados en carpetas de SharePoint
        if self.sharepoint_token=="":
            self.getToken()

        fileName=file.split('\\')[-1]
        print("fileName",fileName)
        folderSP = folderSP.replace("./","/")
        url = "https://"+self.sharepoint_tenant_name+".sharepoint.com/sites/"+site+"/_api/web/GetFolderByServerRelativeUrl('" + folderSP + "')/Files/add(url='" + fileName + "',overwrite=true)"
        headers = {
            'Authorization': "Bearer "+self.sharepoint_token,
            'Content-Type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache",    
            }
        try:
            with open(file,'rb') as payload:
                response = requests.request("POST", url, headers=headers, data=payload)
            print(response.url)
        except:
            raise Exception("error subiendo archivo al sharepoint")